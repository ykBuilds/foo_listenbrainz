#pragma once

#include <string>
#include <map>
#include "pfc.h"
#include "foobar2000.h"

namespace foo_listenbrainz {
	class lbz_http_client
	{
	public:
		static void lbz_http_client::post_url(const char *host, const char *object,
			const char *header, const char *data, abort_callback &callback = abort_callback_dummy());
	};
}
