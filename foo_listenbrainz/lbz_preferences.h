#pragma once

#include "foobar2000.h"

namespace foo_listenbrainz {
	namespace lbz_preferences {
		extern const char *m_server_url;
		extern cfg_bool m_listen_enable;
		extern cfg_string m_user_name;
		extern cfg_string m_user_token;
	}
}
