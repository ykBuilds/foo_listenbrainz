#pragma once

#define _WIN32_WINNT _WIN32_WINNT_WINXP
#define _SECURE_ATL 1

#include "resource.h"
#include "foo_listenbrainz.h"
#include "lbz_http_client.h"
#include "lbz_timer.h"
#include "lbz_listen.h"
#include "lbz_preferences.h"
#include "pfc.h"
#include "jansson.h"
#include "foobar2000.h"
#include "helpers.h"
#include "ATLHelpers.h"
