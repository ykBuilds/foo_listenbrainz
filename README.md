# foo_listenbrainz

foobar2000 plugin to submit listen history to the listenbrainz server.

Status: alpha.

[+] submits listens while online

[+] provides UI to configure user name and token

[-] no checks for online status, no exception handling

[-] no caching of the listens history while offline

[-] configurable server url

[-] no separate thread for network requests

note: I am ultimate C++ noob; some of the missing features are not going to be implemented soon